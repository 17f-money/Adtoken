---
title: What is Adtoken?
date: 2017-11-14
---

Since there are many problems about malavertisements and frauds in the digital advertising market, Adtoken is invented to solve these problems. 

Adtoken is a set of interoperable open protocols built on the Public Ethereum blockchain and specifically targets at the Digital Advertising Industry.  

The adChain registry is the central part of Adtoken, it is a collaboration of ConsenSys, MetaX, and Data & Marketing Association.
AdChain registry is a decentrally-owned domain whitelist. Digital publishers must get majority of vote from the adTokens who have an active role in determining whether the publishers will get access to the adChain registry. Therefore, the adChain registry is a smart contract based on Ethereum blockchain that stores domain names that is approved by adtoken holders. 

Under these following circumstances, incentives will be rewarded to Adtoken holders:
1. The Adchain registry should be kept clean of bot traffic so that more advertisiers will service bid requests from the registrants. 
2. Consequently, registrants are motivated to keep renewing their listings and encourage other publishers to apply for the adChain registry. 